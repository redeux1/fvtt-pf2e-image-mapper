# Changelog
## 1.1.0
- Added option for default regex. Could be useful to help filter which filenames did not result in a match. 

## 1.0.0
- Initial release