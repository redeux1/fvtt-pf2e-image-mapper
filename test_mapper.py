from mapper import Config, NPCRecord, extract_npc_data


def test_search_filenames():
    '''
    Testing various cases of regex matching of filename
    '''
    config = Config("tests/config.ini")
    npc1 = NPCRecord("raven", config)
    npc2 = NPCRecord("raven-swarm", config)
    npc3 = NPCRecord("ravener", config)
    npc4 = NPCRecord("zombie-shambler", config)
    assert npc1.search_filenames([
        "raven.webp", "raven-swarm.webp", "ravener.webp"], npc1.filename) == ["raven.webp"]
    assert npc2.search_filenames([
        "raven.webp", "raven-swarm.webp", "ravener.webp"], npc2.filename) == ["raven-swarm.webp"]
    assert npc3.search_filenames([
        "raven.webp", "raven-swarm.webp", "ravener.webp"], npc3.filename) == ["ravener.webp"]
    assert npc4.search_filenames([
        "zombie-shambler-1.webp", "zombie-shambler-2.webp", "zombie-owlbear.webp"], npc4.filename) == ["zombie-shambler-1.webp", "zombie-shambler-2.webp"]
    assert npc4.search_filenames([
        "zombie-shambler-1_2.0x.webp", "zombie-shambler-2.webp", "zombie-owlbear.webp", "zombie-shambler-troop.webp"], npc4.filename) == ["zombie-shambler-1_2.0x.webp", "zombie-shambler-2.webp"]


def test_set_scale():
    '''
    Testing various cases of regex matching of scale
    '''
    config = Config("tests/config.ini")
    npc1 = NPCRecord("raven", config)
    assert npc1.set_scale(npc1.filename, ["raven.webp"]) == None
    assert npc1.set_scale(npc1.filename, ["raven_3.0x.webp"]) == 3.0
    assert npc1.set_scale(npc1.filename, ["raven_4x.webp"]) == 4.0
    assert npc1.set_scale(npc1.filename, ["raven_5.45x.webp"]) == 5.45
    assert npc1.set_scale(npc1.filename, ["raven-1-6x.webp"]) == 6
    assert npc1.set_scale(npc1.filename, ["raven-2_7.webp"]) == None
    assert npc1.set_scale(npc1.filename, ["raven-3 8x.webp"]) == 8
    assert npc1.set_scale(
        npc1.filename, ["raven-1.webp", "raven-2.webp", "raven-3.webp"]) == None
    assert npc1.set_scale(npc1.filename, ["raven-10.0X.webp"]) == 10.0


def test_set_actor_filepath():
    '''
    Testing construction of actor filepath
    '''
    config = Config("tests/config.ini")
    npc1 = NPCRecord("raven", config)
    assert npc1.set_actor_filepath(npc1.filename, config, [
                                   "raven-1.webp", "raven-2.webp", "raven-3.webp"]) == "pf2/art/bestiary/raven-1.webp"


def test_set_token_filepath():
    '''
    Testing construction of token filepath
    '''
    config = Config("tests/config.ini")
    npc1 = NPCRecord("raven", config)
    assert npc1.set_token_filepath(npc1.filename, config, [
                                   "raven-1.webp"]) == ("pf2/art/bestiary/tokens/raven-1.webp", None)
    assert npc1.set_token_filepath(npc1.filename, config, [
                                   "raven-1.webp", "raven-2.webp", "raven-3.webp"]) == ("pf2/art/bestiary/tokens/raven*.webp", True)
    assert npc1.set_token_filepath(npc1.filename, config, [
                                   "raven-1_2.0X.webp", "raven-2-2x.webp", "raven-3 2.0x.webp"]) == ("pf2/art/bestiary/tokens/raven*.webp", True)


def test_extract_npc_data():
    '''
    Testing extraction of npc data from .json
    '''
    config = Config("tests/config.ini")
    assert extract_npc_data("tests/raven.json", "pathfinder-bestiary-2", "raven", config) == [{"Directory": "pathfinder-bestiary-2", "File Name": "raven",
                                                                                               "_id": "hFbfXmjMkGEzMkQ1", "Actor Art": "pf2/art/bestiary/raven.webp", "Token Art": "pf2/art/bestiary/tokens/raven.webp", "randomImg": None, "scale": None}]
