# fvtt-pf2e-image-mapper
## Description:

This is a utility to generate an image-mapping.json file for [FoundryVTT's](https://foundryvtt.com/) [Pathfinder2e system](https://foundryvtt.com/packages/package/pf2e) and potentially other systems if the user has access to unpacked .json files and the system has implemented the same image-mapping logic. 

## Overview:

The utility has three main functions (via CLI arguments):
<ol>
<li> <code>--existing</code>: This will take an existing image-mapping.json and extract its contents into a CSV. </li>
<li><code>--extract</code>: This iterates through all .jsons in a directory to identify NPC sheets and lists them into a CSV. It will either append to an existing CSV or create a new one. </li>
<li><code>--create</code>: This takes the output.csv from the first two options and will generate the image-mapping.json</li>
</ol>

## Usage:

This section will discuss the set-up and steps for each of the CLI arguments. 

### Config.ini
The [Config.ini](./Config.ini) contains options for the utility. Depending on the CLI argument used, different fields will be required in the config.ini. The config.ini file was chosen instead of argparse because the idea is that as the pf2e system evolves, the image-mapping.json can continually be converted to csv, appended with additional content, and then converted back to an image-mapping.json. The config.ini simplifies subsequent runs of the utility. The options are:
<ol>
<li> [EXISTING]

- <code>IMAGE MAPPING FILEPATH</code>. The full filepath of your existing image-mapping.json. Example = <code>C:\Users\your_user_name\FoundryVTT\Data\modules\your_module_name\image-mapping.json</code>
<li> [EXTRACT] 

-    <code>JSON PACK DIRECTORY</code>. The directory filepath where the unpacked .jsons are stored. Typically this would be the packs folder within your cloned pf2e repository. Example = <code>C:\Users\your_user_name\master\packs</code>
-    <code>ACTOR ART</code>. The directory filepath where your actor art is stored, relative to your server's foundry data folder. Example = <code>pf2\art\bestiary</code>
-    <code>TOKEN ART</code>. The directory filepath where your token art is stored, relative to your server's foundry data folder. Example = <code>pf2\art\bestiary\tokens</code>
-    <code>REGEX MATCH</code> (Boolean). If <code>True</code> then actor and token images will be regex'ed against the specified directories. This must be set to <code>True</code> if you would like support for wildcard or token scaling within <code>--existing</code> functionality.  If <code>False</code>, then actor and token images will be populated based on kebab case npc-names (army-ant-swarm.webp).
-    <code>ACTOR DIRECTORY</code> (required if <code>Regex Match = True</code>). The full local directory filepath where your actor art is stored. This directory will be searched to match actor images to actors. Example = <code>C:\Users\your_user_name\FoundryVTT\Data\pf2\art\bestiary</code>
-    <code>TOKEN DIRECTORY</code> (required if <code>Regex Match = True</code>). The full local directory filepath where your token art is stored. This directory will be searched to match token images to tokens. Example = <code>C:\Users\your_user_name\FoundryVTT\Data\pf2\art\bestiary\tokens</code>
-    <code>REGEX DEFAULT</code> (required if <code>Regex Match = True</code>). A default to apply if no regex match is found. Leave as <code>None</code> to construct an image path based on kebab case npc-names, or insert an image path relative to the server's foundry data folder Example = <code>systems/pf2e/icons/default-icons/npc.svg</code>

</ol>

### CLI arguments

#### <code>--existing</code>
This will take an existing image-mapping.json and extract its contents into a CSV. Ensure that the <code> IMAGE MAPPING FILEPATH</code> is set in the Config.ini and then run the utilty with a CLI argument: <code>python mapper.py --existing</code>. After the completion of the script there will be an output.csv file in the script's directory.
#### <code>--extract</code>
This iterates through all .jsons in a directory to identify NPC sheets and lists them into a CSV. It will either append to an existing CSV or create a new one. Ensure that the Config.ini <code>[EXTRACT]</code> fields are completed and then run the script with a CLI argument: <code>python mapper.py --extract</code>. After the completion of the script there will either be a new or appended output.csv file in the script's directory. 

If <code>Regex Match = False</code> then this command will assign actor art and token art to the specified directories, and the file name is populated as a kebab-case.webp file. For example a Army Ant Swarm is assumed to have a army-ant-swarm.webp. 

If <code>Regex Match = True</code> then this command will search your actor and token images for regex matches and assign them to actor/tokens. If no match is found then it defaults to the kebab-case.webp based on file name. Actors will be assigned the first match, and tokens will be assigned a wildcard match if multiple matches are found. If matches are found it will also search for scaling in the file name. The scaling is determined by a number or decimal followed by an "x" (case-insensitive. For example the following would be valid scaling matches: <code>army-ant-swarm_2x.webp, army-ant-swarm_2.0x.webp, army-ant-swarm-2.0X.webp,</code>. In each of these examples, token scale would be set to <code>"2"</code>. 
#### <code>--create</code>
This takes the output.csv from the first two options and will generate the image-mapping.json. This has no config.ini requirements and can be run with a CLI argument: <code>python mapper.py --create</code>. After the completion of the script there will be an image-mapping.json in the script's directory. 

### Practical workflow ideas
The script was set up to be relatively flexible. If you have an existing image-mapping.json then you can run the <code>--existing</code> CLI argument to produce an output.csv. Next you can run the <code>--extract</code> CLI argument to populate additional NPC records in the output.csv. The <code>--extract</code> argument can be run first if you do not have an existing image-mapping.json. 

These functions were intentionally set up to produce a .csv file to help review the data, and manually update the data, though I would encourage formatting files properly so that the utility can auto set the data on subsequent runs (and not lose your progress).

Once you have run one or both of the previous commands, you can optionally open the output.csv and begin updating records based on your needs. Once you have finalized any manual updates then you can run the <code>--create</code> argument to produce the image-mapping.json. 

