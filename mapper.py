import os
import re
import csv
import json
import argparse
import configparser


class Config:
    def __init__(self, config_file):

        # Read config.ini
        self.config = configparser.ConfigParser()
        self.config.read(config_file)

        # Set config variables
        self.input_directory = self.config.get(
            'EXTRACT', 'JSON PACK DIRECTORY')
        self.actor_images = self.config.get(
            'EXTRACT', 'ACTOR ART')
        self.token_images = self.config.get(
            'EXTRACT', 'TOKEN ART')
        self.actor_directory = self.config.get(
            'EXTRACT', 'ACTOR DIRECTORY')
        self.token_directory = self.config.get(
            'EXTRACT', 'TOKEN DIRECTORY')
        self.regex_match = self.config.getboolean(
            'EXTRACT', 'REGEX MATCH')
        self.regex_default = self.config.get(
            'EXTRACT', 'REGEX DEFAULT')
        self.existing_json_file = self.config.get(
            'EXISTING', 'IMAGE MAPPING FILEPATH')
        self.output_csv_file = "output.csv"

        # if regex match is true, generate list for actor and token images.
        if self.regex_match == True:
            self.actor_list = generate_file_list(self.actor_directory)
            self.token_list = generate_file_list(self.token_directory)


class NPCRecord:
    '''
    The NPC object containing data needed to write to the output.csv
    '''

    def __init__(self, file_name, config):
        # filename
        self.filename = file_name

        # if regex match, construct matched lists.
        if config.regex_match == True:
            matched_actor_list = self.search_filenames(
                config.actor_list, file_name)
            matched_token_list = self.search_filenames(
                config.token_list, file_name)

        # actor filepath
        self.actor_filepath = self.set_actor_filepath(
            file_name, config, matched_actor_list)

        # token filepath and randomImg
        self.token_filepath, self.randomImg = self.set_token_filepath(
            file_name, config, matched_token_list)

        # scale. if regex match is true, search for scale in filename. otherwise scale is ""
        if config.regex_match == True:
            self.scale = self.set_scale(file_name, matched_token_list)
        else:
            self.scale = ""

    def set_actor_filepath(self, file_name, config, matched_file_list):
        '''
        Constructs the actor's image filepath
        '''
        # if regex match is true, then used matched file list.
        if config.regex_match == True:
            # If 1 or more names in list is found then return first match as file_name and construct actor filepath
            if len(matched_file_list) > 0:
                file_name = matched_file_list[0]
                actor_filepath = os.path.join(config.actor_images, file_name)
                actor_filepath = actor_filepath.replace('\\', '/')
                return actor_filepath

            # if no regex match, and regex default is not "None", then apply default
            if config.regex_default != "None":
                actor_filepath = os.path.join(config.regex_default)
                actor_filepath = actor_filepath.replace('\\', '/')
                return actor_filepath

        # if regex match is true, or if search_filenames returned no results, then use default file_name
        actor_filepath = os.path.join(config.actor_images, file_name + ".webp")
        actor_filepath = actor_filepath.replace('\\', '/')
        return actor_filepath

    def set_token_filepath(self, file_name, config, matched_file_list):
        '''
        Constructs the token's image filepath and determines if wildcard (randomImg) applies. 
        '''
        # if regex match is true, then used matched file list.
        if config.regex_match == True:
            # If 1 or more names in list is found then return * for wildcard
            if len(matched_file_list) == 1:
                file_name = matched_file_list[0]
                token_filepath = os.path.join(config.token_images, file_name)
                token_filepath = token_filepath.replace('\\', '/')
                return token_filepath, None
            elif len(matched_file_list) > 1:
                non_extension, extension = os.path.splitext(
                    matched_file_list[0])
                token_filepath = os.path.join(
                    config.token_images, file_name + "*" + extension)
                token_filepath = token_filepath.replace('\\', '/')
                return token_filepath, True

            # if no regex match, and regex default is not "None", then apply default
            if config.regex_default != "None":
                token_filepath = os.path.join(config.regex_default)
                token_filepath = token_filepath.replace('\\', '/')
                return token_filepath, None

        # if regex match is true, or if search_filenames returned no results, then use default file_name
        token_filepath = os.path.join(config.token_images, file_name + ".webp")
        token_filepath = token_filepath.replace('\\', '/')
        return token_filepath, None

    def set_scale(self, file_name, matched_token_list):
        """
        Determines if token image should have non-default scale.

        """
        # searches first record in matched_token_list for _2.0x where decimal is not required, and X is case insentive.
        # other valid input examples _3x or _3.0X. The preceding underscore IS required.
        scale_pattern = r"_?(\d+(?:\.\d+)*)(?:x|X)"

        if len(matched_token_list) >= 1:
            if matches := re.search(scale_pattern, matched_token_list[0]):
                scale = float(matches.group(1))
                return scale

        return None

    def search_filenames(self, filenames_list, filename_npc):
        '''
        Uses regex pattern to match against the filename at the start of the string. 
        '''
        # the filename of npc must be in kebab case and at start of string
        # after the filename the next characters need to be - or _ or .
        # This is to avoid having something like Raven match with Ravener
        # Lastly the filename cannot include swarm unless the npc is a swarm. This is to prevent Raven from matching with Raven Swarm, but Raven Swarm can still produce a match.
        regex_pattern = re.compile(
            # r'^' + re.escape(filename_npc) + r'[-_.](?!.*\bswarm\b).*', re.IGNORECASE)
            r'^(' + re.escape(filename_npc) + r'[-_.])(?!.*\b(?:swarm)|(?:army)|(?:troop)|(?:brigade)\b).*', re.IGNORECASE)

        matched_filenames = []
        for filename in filenames_list:
            if regex_pattern.match(filename):
                # print(regex_pattern, regex_pattern.match(filename)) # debugging. this will be a LOT of printing if you have a large asset folder you're iterating through.
                matched_filenames.append(filename)

        return matched_filenames


def main():
    # Establish config
    config = Config('config.ini')

    # testing regex
    # file_list = (generate_file_list(config.token_directory))
    # print(search_filenames(file_list, "zombie-shambler"))

    # Establish CLI arguments
    parser = argparse.ArgumentParser(
        description="Extract NPC records or build image-mapping.json")
    parser.add_argument("--extract", action="store_true",
                        help="Extract NPC records from unpacked .jsons to CSV")
    parser.add_argument("--create", action="store_true",
                        help="Create image-mapping.json from extracted CSV")
    parser.add_argument("--existing", action="store_true",
                        help="Convert existing image-mapping.json to CSV")

    args = parser.parse_args()

    # Determine Mode
    if args.extract:
        if not config.config.has_section('EXTRACT'):
            print("Error: 'EXTRACT' section missing in config.ini.")
            return
        # Begin extraction of NPC records
        extract_npc_records(config)
        print("NPC records successfully extracted to output.csv")

    if args.create:
        # Read the CSV file
        csv_file = "output.csv"
        records = read_from_csv(csv_file)

        # Build the image mapping dictionary
        image_mapping = build_image_mapping(records)

        # Write the dictionary to image-mapping.json
        json_file = "image-mapping.json"
        write_to_json(json_file, image_mapping)
        print("output.csv contents successfully written to image-mapping.json")

    if args.existing:
        if not config.config.has_section('EXISTING'):
            print("Error: 'EXISTING' section missing in config.ini.")
            return

        # Read the existing image-mapping.json
        records = read_from_json(config.existing_json_file)

        # Convert records to the format for CSV
        csv_records = convert_to_csv_format(records)

        # Write the records to output.csv
        config.output_csv_file = "output.csv"
        write_to_csv(config.output_csv_file, csv_records)
        print(
            f"{config.existing_json_file} contents successfully extracted to output.csv")


# EXTRACT


def process_json_files(directory):
    '''
    Creates a list of json files
    '''
    json_files = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".json"):
                json_files.append(os.path.join(root, file))
    return json_files


def extract_npc_records(config):
    '''
    Processes json files so NPC data can be extracted from them and written to CSV. 
    '''

    json_files = process_json_files(config.input_directory)
    npc_records = []

    # Esstablish list of files and call extract_npc_data on each.
    for json_file in json_files:
        _, file_name = os.path.split(json_file)
        file_name = os.path.splitext(file_name)[0]
        directory_name = os.path.basename(os.path.dirname(json_file))
        npc_records.extend(extract_npc_data(
            json_file, directory_name, file_name, config))

    # Load existing records from the CSV if it exists
    if os.path.exists(config.output_csv_file):
        existing_records = read_from_csv(config.output_csv_file)
        existing_ids = {record["_id"] for record in existing_records}
        npc_records = [
            record for record in npc_records if record["_id"] not in existing_ids]

        # Append new records to the existing CSV file
        append_to_csv(config.output_csv_file, npc_records, config)
    else:
        # If the CSV doesn't exist, create a new one
        write_to_csv(config.output_csv_file, npc_records)


def extract_npc_data(json_file, directory_name, file_name, config):
    '''
    Processes the individual json files and extracts data. 
    '''
    # print(config.actor_images)

    with open(json_file, "r", encoding="utf-8") as file:
        try:
            data = json.load(file)
        except json.JSONDecodeError:
            print(
                f"Skipping file: {json_file} (Unable to parse as JSON object)")
            return []

    npc_records = []

    # Check if the data is a list. This could occur if multiple NPC records are in a single .json.
    if isinstance(data, list):
        for record in data:
            # Verify that the record is a dictionary, has "_id" key, and "type" is "npc"
            if isinstance(record, dict) and record.get("_id") and record.get("type") == "npc":
                # Create NPC object
                npc = NPCRecord(file_name, config)
                # Append NPC data to npc_records.
                npc_records.append({
                    "Directory": directory_name,
                    "File Name": file_name,
                    "_id": record["_id"],
                    "Actor Art": npc.actor_filepath,
                    "Token Art": npc.token_filepath,
                    "randomImg": npc.randomImg,
                    "scale": npc.scale
                })
    # Check if data is in dictionary. This is the typical expectation (single NPC record in a .json)
    elif isinstance(data, dict) and data.get("_id") and data.get("type") == "npc":
        # Create NPC object
        npc = NPCRecord(file_name, config)
        # Append NPC data to npc_records.
        npc_records.append({
            "Directory": directory_name,
            "File Name": file_name,
            "_id": data["_id"],
            "Actor Art": npc.actor_filepath,
            "Token Art": npc.token_filepath,
            "randomImg": npc.randomImg,
            "scale": npc.scale
        })
    # print(npc_records)  # verifying npc_records is returning expected format.
    return npc_records


def generate_file_list(directory):
    '''
    Returns a list of files within a directory
    '''
    try:
        file_names = os.listdir(directory)
        return file_names
    except OSError:
        print(f"Error: Unable to list files in directory '{directory}'")
        return []


def write_to_csv(csv_file, npc_records):
    '''
    Writes to output.csv
    '''
    fieldnames = ["Directory", "File Name", "_id",
                  "Actor Art", "Token Art", "randomImg", "scale"]
    with open(csv_file, "w", newline='', encoding='utf-8') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        if file.tell() == 0:
            writer.writeheader()
        writer.writerows(npc_records)


def append_to_csv(csv_file, npc_records, config):
    '''
    Appends to output.csv instead of overwriting. 
    '''
    with open(csv_file, "a", newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        for record in npc_records:
            writer.writerow([record["Directory"], record["File Name"], record["_id"],
                            record["Actor Art"], record["Token Art"], record["randomImg"], record["scale"]])


# EXISTING
def read_from_json(json_file):
    '''
    Opens .json and returns data
    '''
    with open(json_file, "r", encoding="utf-8") as file:
        data = json.load(file)
    return data


def convert_to_csv_format(records):
    '''
    Processes the image-mapping data so it can be written to csv
    '''
    csv_records = []

    # Iterate through directories and NPC records.
    for directory, entries in records.items():
        for _id, entry in entries.items():
            # Set actor art
            actor_art = entry.get("actor", "")

            # Check if the "token" entry is a dictionary
            if isinstance(entry.get("token"), dict):
                # If it's a dictionary, get its values
                token_dict = entry["token"]
                token_art = token_dict.get("img", "")
                random_img = token_dict.get("randomImg", "")
                scale = token_dict.get("scale", "")
            else:
                # If "token" is not a dictionary, set "Token Art" and other values
                # Note: Since there's no "randomImg" or "scale", we directly assign the "token" value to "Token Art"
                token_art = entry.get("token", "")
                random_img = ""
                scale = ""

            # Append the NPC information to the CSV records
            csv_records.append({
                "Directory": directory,
                "File Name": "",
                "_id": _id,
                "Actor Art": actor_art,
                "Token Art": token_art,
                "randomImg": random_img,
                "scale": scale
            })

    return csv_records


# CREATE
def read_from_csv(csv_file):
    '''
    Opens output.csv and returns the records
    '''
    records = []
    with open(csv_file, "r", newline='', encoding='utf-8') as file:
        reader = csv.DictReader(file)
        for row in reader:
            records.append(row)
    return records


def build_image_mapping(records):
    '''
    Constructs the image-mapping dictionaries
    '''
    image_mapping = {}

    # Iterate through CSV records and populate the image mapping
    for record in records:
        directory = record["Directory"]
        _id = record["_id"]
        actor_art = record["Actor Art"]
        token_art = record["Token Art"]
        random_img = bool(record["randomImg"])  # convert to boolean
        # Convert to float or None
        scale = float(record["scale"]) if record["scale"] != "" else None

        # Initialize the entry for the directory if it doesn't exist
        if directory not in image_mapping:
            image_mapping[directory] = {}

        # Populate the entry for the _id
        image_mapping[directory][_id] = {
            "actor": actor_art,
            "token": {}
        }

        # Check if randomImg and/or scale have values
        if random_img or scale:
            token_entry = {"img": token_art}

            if random_img:
                token_entry["randomImg"] = random_img

            if scale:
                token_entry["scale"] = scale

            image_mapping[directory][_id]["token"] = token_entry
        else:
            # If neither randomImg nor scale have values, just pass token_art
            image_mapping[directory][_id]["token"]["img"] = token_art

    return image_mapping


def write_to_json(json_file, data):
    '''
    Writes image-mapping to .json
    '''
    with open(json_file, "w") as file:
        json.dump(data, file, indent=4)


if __name__ == "__main__":
    main()
